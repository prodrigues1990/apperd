from flask import Flask, render_template, request

from . import projects

app = Flask(__name__)

@app.route('/projects', methods=['GET'])
def get_projects():
    return render_template('projects.html', projects=projects.search())

@app.route('/projects', methods=['POST'])
def post_project():
    projects.add(request.form['name'])

    return get_projects()
