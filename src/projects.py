import os
import subprocess
from pathlib import Path

REPOS = Path(os.environ.get('GIT_REPOS_DIR', '/home/git/git/'))

def search():
    return [{'name': file.name} for file in REPOS.iterdir()]

def add(name):
    repo = f'{name}.git'
    path = Path(REPOS, repo)

    path.mkdir(parents=True, exist_ok=False)
    subprocess.Popen(['git', 'init', '--bare', repo], cwd=REPOS)

    # @TODO: do this with setfacl command in container start
    subprocess.Popen(
        ['ln', '--symbolic', '-T', '/apperd/git/post-receive', 'hooks/post-receive'],
        cwd=path,
    )
    subprocess.Popen(['chown', '-R', 'git', repo], cwd=REPOS)
    subprocess.Popen(['chgrp', '-R', 'git', repo], cwd=REPOS)
    subprocess.Popen(['chmod', '-R', '770', repo], cwd=REPOS)
