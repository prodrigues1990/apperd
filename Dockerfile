FROM python:3.7-slim

RUN apt-get update -yqq && \
    apt-get -y install \
        openssh-server \
        git \
        acl

# set up a user to access git repositories
RUN adduser --gecos "" --shell /usr/bin/git-shell --disabled-password git
RUN mkdir /home/git/.ssh

# set up a directory for git repositories
RUN mkdir /home/git/git

# set up sshd
RUN mkdir /var/run/sshd
COPY git/sshd_config /etc/ssh/sshd_config

# set up apperd app
COPY . /apperd
RUN mkdir /var/apperd
RUN pip install -r apperd/requirements.txt

# set up temporary artifact directory
RUN mkdir /tmp/apperd

# set up a test repo
RUN git init --bare /home/git/git/test.git
RUN cp /apperd/git/post-receive /home/git/git/test.git/hooks/post-receive

EXPOSE 22

CMD "/apperd/start.sh"
