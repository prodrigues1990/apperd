#!/bin/bash

# generate sshd server keys, if not already present
ssh-keygen -A

# fix access to ssh files in mounting volumes
chown git:git /home/git/.ssh/authorized_keys
chmod 600 /home/git/.ssh/authorized_keys

# fix git repository permissions on mounting volumes
find /home/git/git -name *.git | xargs chown -R git
find /home/git/git -name *.git | xargs chgrp -R git
find /home/git/git -name *.git | xargs chmod -R 770

/usr/sbin/sshd
cd /apperd && gunicorn wsgi:app -b 0.0.0.0:80
