
## Docker

### Set up the dev machine

1. Create an authorized_keys file for sshd
```bash
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
```

2. Add a place to store git repositories
```bash
mkdir ~/git
```

### Build the container
```bash
docker build --tag apperd .
```

### Run the container
Change `~/projects/apperd` to the location of the project on your system.
```bash
docker run -d --name apperd \
    -p 22:22/tcp \
    -p 80:80 \
    -v ~/.ssh/authorized_keys:/home/git/.ssh/authorized_keys \
    -v ~/git:/home/git/git \
    -v ~/projects/apperd:/apperd \
    apperd:latest
```
