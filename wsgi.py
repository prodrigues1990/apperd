import os
from src.web import app

if __name__ == '__main__':
    host = os.environ.get('HOST', '0.0.0.0')
    port = os.environ.get('PORT', '5000')
    
    app.run(debug=True, host=host, port=int(port))
